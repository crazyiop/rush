# Quick PDF Link

If you land on this page and are just looking for the small pdf files to play more puzzle, this is it:
 - [200 hard puzzles with normal game pieces](normal-small.pdf)
 - [200 hard puzzles with 2 1x1 DIY bloquer](blocker-small.pdf)

# graphics
truck: https://www.freepik.com/free-vector/top-view-flat-cars-trucks_1349627.htm
cars: https://www.freepik.com/free-vector/simple-flat-car-collection_1349613.htm
barrier: https://www.flaticon.com/free-icon/barrier_1166550
