from random import choice, randint
from PIL import Image, ImageFont, ImageDraw


from cards import (
    mk_content,
    load_db,
    get_color,
)
from card_printing.cards import (
    Card,
    Deck,
    PATH,
)


def mk_header(big, little):
    header = Image.new("RGBA", (Card.SAFE[0], 100))
    draw = ImageDraw.Draw(header)

    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 100)
    nl, nt, nr, nb = draw.textbbox((0, 0), str(big), font=font)
    draw.text((0, 0), str(big), (0, 0, 0), font=font)

    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 50)
    text = f" {little}"
    l, t, r, b = draw.textbbox((0, 0), text, font=font)
    draw.text((nr, nb - b), text, (100, 100, 100), font=font)
    return header


def mk_footer(n):
    footer = Image.new("RGBA", (Card.SAFE[0], 100))
    draw = ImageDraw.Draw(footer)

    font = ImageFont.truetype(str(PATH / "Graduate-Regular.ttf"), 50)
    l, t, r, b = draw.textbbox((0, 0), str(n), font=font)
    draw.text(((Card.SAFE[0] - r) / 2, 0), str(n), (0, 0, 0), font=font)
    return footer


def gen_cards(nb, blocker=False):
    cache = {}
    selected = []
    if blocker:
        mwall = 1
        Mwall = 2
    else:
        mwall = Mwall = 0

    while len(selected) < nb:
        lvl = randint(15, 60)
        if lvl not in cache:
            try:
                data = load_db(lvl, limit_wall=Mwall, min_wall=mwall)
            except FileNotFoundError:
                continue
            if not len(data):
                continue
            cache[lvl] = data

        new = choice(cache[lvl])
        if new in selected:
            continue

        elems, (lvl, idx, nb_car, nb_truck, nb_wall, nb_states) = new
        if blocker and not nb_wall:
            continue
        if not blocker and nb_wall:
            continue

        selected.append(new)

    def _key(puzzle):
        elems, (lvl, idx, nb_car, nb_truck, nb_wall, nb_states) = puzzle
        return lvl, idx

    selected.sort(key=_key)
    for page_number, new in enumerate(selected, start=1):
        elems, (lvl, idx, nb_car, nb_truck, nb_wall, nb_states) = new

        header = mk_header(big=lvl, little=idx)

        chars = iter("BCDEFGHIJKLMNOPQRSTUVWXYZ")
        for v in elems:
            if v["pos"][1] == 2 and v["orientation"] == "-":
                v["char"] = "A"
            else:
                v["char"] = next(chars)
        get_color(elems)
        content = mk_content(elems)
        footer = mk_footer(page_number)
        yield Card(
            parts=[header, content, footer],
        )


if __name__ == "__main__":
    d = Deck("100 random puzzles with blockers")
    for c in gen_cards(100, True):
        d.add(c)
    d.pdf()
    d = Deck("100 random puzzles without blockers")
    for c in gen_cards(100, False):
        d.add(c)
    d.pdf()
