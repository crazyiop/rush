from random import choice, shuffle, randrange, seed
from collections import defaultdict
from itertools import cycle
from PIL import Image, ImageFont, ImageDraw, ImageColor
from mkgraphics import colors
import subprocess
import shlex
import re
import qrcode
from collections import deque

from pathlib import Path


from card_printing.cards import (
    Card,
    mk_md,
    img_merge,
)


def to_puzzle(s, idx):
    # parse string for databas.txt into usefull data
    lvl, grid, states = s.split()
    lvl = int(lvl)

    states = int(states)

    parts = defaultdict(list)
    nb_car, nb_truck, nb_wall = 0, 0, 0
    for i, c in enumerate(grid):
        if c != "o":
            if c == "x":
                nb_wall += 1
                c = "x" + str(nb_wall)
            parts[c].append(i)

    elements = []
    for k, v in parts.items():
        y, x = divmod(v[0], 6)
        e = {"type": {1: "barrier", 2: "car", 3: "truck"}[len(v)], "pos": (x, y)}
        if e["type"] == "barrier":
            e["orientation"] = "|"
        else:
            e["orientation"] = "|-"[v[1] - v[0] == 1]
            if e["type"] == "car":
                nb_car += 1
            else:
                nb_truck += 1
        elements.append(e)

    return elements, (lvl, idx, nb_car, nb_truck, nb_wall, states)


def load_db(lvl, limit_car=12, limit_truck=4, limit_wall=0, min_wall=0):
    print(f"reading {lvl} db file")
    puzzles = []
    with open(Path("db") / f"{lvl}.txt", "r") as db:
        for idx, line in enumerate(db.readlines(), start=1):
            line.strip()
            elements, infos = to_puzzle(line, idx)
            _, _, nb_car, nb_truck, nb_wall, nb_states = infos
            if nb_wall < min_wall:
                continue
            if (
                nb_car <= limit_car
                and nb_truck <= limit_truck
                and nb_wall <= limit_wall
            ):
                puzzles.append((elements, infos))
    return puzzles


def get_color(elements):
    available_colors = []
    # fix random
    seed("".join(str(v["pos"]) for v in sorted(elements, key=lambda x: x["pos"])))
    for v in elements:
        if len(available_colors) == 0:
            # When all colors are used refill the pool
            available_colors = list(colors.keys())
            available_colors.remove("red")  # reserved for primary car
            shuffle(available_colors)

        if v["type"] == "barrier":
            color = ""
            flip = False
            subtype = ""
        else:
            if v["pos"][1] == 2 and v["orientation"] == "-":
                subtype = "primary"
                flip = True
                color = "red"
            else:
                if v["type"] == "car":
                    subtype = str(randrange(7))
                else:
                    subtype = ""
                color = available_colors.pop()
                flip = choice([True, False])

        v["color"] = color
        v["flip"] = flip
        v["subtype"] = subtype


def get_car_img(v, unit):
    offsets = {
        "truck": {"-": (0, unit), "|": (unit, 0)},
        "car": {"-": (unit // 2, unit), "|": (unit, unit // 2)},
        "barrier": {"-": (0, 0), "|": (0, 0)},
    }
    car_mask = Image.new("RGBA", (3 * unit, 3 * unit))
    if v["type"] == "barrier":
        car = Image.open(Path("graphics", "generated", f"{v['type']}.png"))
    else:
        car = Image.open(
            Path(
                "graphics",
                "generated",
                f"{v['type']}{v['subtype']}-{v['color']}.png",
            )
        )
    # Offset from a centered form for each type of element
    img_merge(car_mask, car, offsets[v["type"]]["|"])
    if v["flip"]:
        car_mask = car_mask.transpose(Image.FLIP_TOP_BOTTOM)
    if v["orientation"] == "-":
        car_mask = car_mask.rotate(90)

    return car_mask


def mk_content(elements, unit=100, debug=True):
    size = (6 * unit, 6 * unit)
    content = Image.new("RGBA", size)
    draw = ImageDraw.Draw(content)
    offsets = {
        "truck": {"-": (0, unit), "|": (unit, 0)},
        "car": {"-": (unit // 2, unit), "|": (unit, unit // 2)},
        "barrier": {"-": (0, 0), "|": (0, 0)},
    }
    if debug:
        draw.rectangle([0, 0, size[0], size[1]], fill="grey")

    grid = Image.open(Path("graphics", "generated", "grid.png"))
    draw = ImageDraw.Draw(grid)

    # Add grid point
    radius = 3
    for x in range(1, 6):
        for y in range(1, 6):
            draw.ellipse(
                (
                    x * unit - radius,
                    y * unit - radius,
                    x * unit + radius,
                    y * unit + radius,
                ),
                fill="grey",
                outline="grey",
            )

    for v in elements:
        car_mask = get_car_img(v, unit)
        img_merge(
            grid,
            car_mask,
            (
                unit * v["pos"][0] - offsets[v["type"]][v["orientation"]][0],
                unit * v["pos"][1] - offsets[v["type"]][v["orientation"]][1],
            ),
        )

    return grid


def mk_cover():
    card = Image.new("RGBA", Card.BLEED)
    unit = 40
    w = Card.BLEED[0] // unit + 3
    h = Card.BLEED[1] // unit + 3
    draw = ImageDraw.Draw(card)
    draw.rectangle((0, 0, *Card.BLEED), fill="white")

    # show window in deck box
    window_box = (189, 336, 558, 945)
    draw.rectangle(window_box, outline="red")
    logo = Image.open("logo.png")
    logo_pos = (
        189 + (558 - 189 - logo.size[0]) // 2,
        336 + (945 - 336 - logo.size[1]) // 2,
    )
    img_merge(card, logo, logo_pos)

    grid = []
    for _ in range(h):
        grid.append(["."] * w)

    # Add grid point
    radius = 0
    for x in range(w + 1):
        for y in range(h + 1):
            if (
                logo_pos[0] // unit - 1 < x < (logo_pos[0] + logo.size[0]) // unit + 1
                and logo_pos[1] // unit - 1
                < y
                < (logo_pos[1] + logo.size[1]) // unit + 1
            ):
                try:
                    grid[y][x] = "x"
                except IndexError:
                    pass
            else:
                draw.ellipse(
                    (
                        x * unit - radius,
                        y * unit - radius,
                        x * unit + radius,
                        y * unit + radius,
                    ),
                    fill="grey",
                    outline="grey",
                )

    it_colors = iter(cycle(list(colors.keys())))
    offsets = {
        "truck": {"-": (0, unit), "|": (unit, 0)},
        "car": {"-": (unit // 2, unit), "|": (unit, unit // 2)},
        "barrier": {"-": (0, 0), "|": (0, 0)},
    }
    for _ in range(1000):
        v = {}
        t = choice(["car", "truck"])
        v["type"] = t
        v["orientation"] = choice("|-")
        x, y = (randrange(0, w), randrange(0, h))
        v["pos"] = x, y
        if v["type"] == "car":
            v["subtype"] = randrange(7)
        else:
            v["subtype"] = ""

        # test if place is free
        if t == "car":
            l = 2
        else:
            l = 3
        if v["orientation"] == "-":
            dx, dy = 1, 0
        else:
            dx, dy = 0, 1

        cx = x
        cy = y
        for _ in range(l):
            try:
                if grid[cy][cx] == "x":
                    break
            except IndexError:
                pass
            cx += dx
            cy += dy
        else:
            # all place free mark them
            cx = x
            cy = y
            for _ in range(l):
                try:
                    grid[cy][cx] = "x"
                except IndexError:
                    pass
                cx += dx
                cy += dy
            v["color"] = next(it_colors)
            v["flip"] = choice([True, False])
            car_mask = get_car_img(v, 100).resize((3 * unit, 3 * unit))
            img_merge(
                card,
                car_mask,
                (
                    unit * v["pos"][0] - offsets[v["type"]][v["orientation"]][0],
                    unit * v["pos"][1] - offsets[v["type"]][v["orientation"]][1],
                ),
            )
    card.show()
    return card


def mk_logo_cover():
    card = Image.new("RGBA", Card.BLEED)
    draw = ImageDraw.Draw(card)
    draw.rectangle((0, 0, *Card.BLEED), fill="#c8d8e4")
    # logo
    logo = Image.open("logobig.png")
    x, y = logo.size
    # target x size
    tx = int(0.85 * Card.CUT[0])
    ratio = tx / x

    logo = logo.resize((int(ratio * x), int(ratio * y)))
    x, y = logo.size
    px = (Card.BLEED[0] - x) // 2
    py = (Card.BLEED[1] - y) // 2
    img_merge(card, logo, (px, py))

    draw.rectangle(
        [
            *Card.cut_margin,
            Card.cut_margin[0] + Card.CUT[0],
            Card.cut_margin[1] + Card.CUT[1],
        ],
        outline="black",
    )

    card.save("cover.png")
    return card


def mk_txt():
    for title, txt in [
        (
            "The Cognitive Learning Kit™",
            """This is part of The Cognitive Learning Kit™ - a cognitive learning material made by Neuroguide.
With these cards it should be possible for all young people to learn without the puzzles being too complex from the beginning.

These cards are developed by Neuroguide. They are compatible with Rush Hour®, but not a part of, Rush Hour® made by ThinkFun.

Enjoy!""",
        ),
        (
            "Den Kognitive Læringskuffert™",
            """Dette er en del af Den Kognitive Læringskuffert™ - et kognitivt læringsmateriale
lavet af Neuroguide. Med disse kort er det muligt for alle børn at udvikle sig
uden af opgavernes kompleksitet bliver for høj.

Disse kort er udviklet af Neuroguide. De kan bruges sammen med Rush Hour®, men er ikke en del af Rush Hour® fra ThinkFun.

God fornøjelse!""",
        ),
    ]:
        card = Image.new("RGBA", Card.BLEED)
        img_merge(card, _mk_descr_header(title), coord=(Card.safe_margin[0], 100))
        img_merge(card, _mk_descr_content(txt), coord=(Card.safe_margin[0], 200))

        qr = qrcode.QRCode(box_size=8)
        qr.add_data("https://neuroguide.dk/den-kognitive-laeringskuffert")
        qr.make(fit=True)
        qr_img = qr.make_image().convert("RGBA")
        print("qr size", qr_img.size)

        m = (Card.BLEED[0] - qr_img.size[0]) // 2

        img_merge(card, qr_img, coord=(m, Card.BLEED[1] - qr_img.size[1] - m // 2))
        yield card


def _mk_descr_header(txt):
    size = Card.SAFE[0]
    font_size = 44

    header = Image.new("RGBA", (size, 100))
    draw = ImageDraw.Draw(header)

    font = ImageFont.truetype("Myriad Pro Bold.ttf", font_size)
    _, _, sx, _ = draw.textbbox((0, 0), txt, font=font)

    startx = (size - sx) // 2
    draw.text((startx, 0), txt, ImageColor.getrgb("#000000"), font=font)
    return header


def _mk_descr_content(markdown):
    card = Image.new("RGBA", (Card.SAFE[0], 600))
    draw = ImageDraw.Draw(card)
    base_font_size = 30
    currentx, currenty = 15, 15
    font = ImageFont.truetype("Myriad Pro Regular.ttf", base_font_size)

    for part in markdown.split("\n\n"):
        text = part.replace("\n", " ")
        words = deque(text.split(" "))
        words.append("EOF")
        while len(words) > 1:
            w = ""
            line = []
            sx = 0
            while sx < Card.SAFE[0] - 30:
                w = words.popleft()
                if w == "EOF":
                    break
                line.append(w)
                _, _, sx, _ = draw.textbbox((0, 0), " ".join(line), font=font)
            if w != "EOF":
                words.appendleft(line[-1])
                line = line[:-1]

            draw.text((currentx, currenty), " ".join(line), "black", font=font)
            currenty += int(base_font_size * 1.35)  # 125% interline

        # skip a line between block of \n\n
        currenty += base_font_size
    return card


def mk_descriptions(data):
    for i in range(2):
        card = Image.new("RGBA", Card.BLEED)
        title = data[0 + i * 2]
        txt = data[1 + i * 2]

        img_merge(card, _mk_descr_header(title), coord=(Card.safe_margin[0], 100))
        img_merge(card, _mk_descr_content(txt), coord=(Card.safe_margin[0], 200))

        yield card


def mk_solution(elems, unit=100, debug=False):
    size = (615, 500)
    content = Image.new("RGBA", size)
    draw = ImageDraw.Draw(content)
    font_arrow = ImageFont.truetype("Arrows.ttf", 40)

    grid = []
    for _ in range(6):
        grid.append(["."] * 6)
    for v in elems:
        x, y = v["pos"]
        l = {"car": 2, "barrier": 1, "truck": 3}[v["type"]]
        if v["orientation"] == "-":
            dx, dy = 1, 0
        else:
            dx, dy = 0, 1

        for _ in range(l):
            grid[y][x] = v["char"]
            x += dx
            y += dy
    data = "".join(["".join(line) for line in grid])

    output = subprocess.check_output(
        shlex.split(f"go run cmd/solve/main.go {data}"),
        cwd="/home/iop/dev/Game/fogelman/rush",
    ).decode("utf8")

    sy = size[1] // 5

    for i, part in enumerate(re.findall(r"[A-Z][+-]\d", output)):
        y, x = divmod(i, 2)
        x = x * size[0] // 2
        y = y * sy
        char, way, n = part
        for v in elems:
            if v["char"] == char:
                break
        n = int(n)
        c = {("|", "-"): "C", ("|", "+"): "D", ("-", "-"): "B", ("-", "+"): "A"}[
            (v["orientation"], way)
        ]
        car_mask = get_car_img(v, unit).resize((sy, sy))
        if debug:
            draw.rectangle(
                [x, y, x + car_mask.size[0], y + car_mask.size[1]], fill="white"
            )
        img_merge(content, car_mask, (x, y))
        for j in range(n):
            _, _, tx, ty = draw.textbbox((0, 0), c, font=font_arrow)
            draw.text(
                (x + sy + 50 * j, y + (sy - ty) // 2),
                c,
                (0, 0, 0),
                font=font_arrow,
            )
    return content


def mk_credits():
    txt = """
    #Made by
    Jonathan Nifenecker, 2022
    """
    return mk_md(txt, "https://gitlab.com/crazyiop")
