import os

try:
    os.makedirs("db")
except:
    pass

with open("rush.txt", "r") as fp:
    data = fp.readlines()

cur = 0
fp = open("/tmp/dummy", "w")

for line in data:
    lvl, *_ = line.split()
    if lvl == cur:
        fp.write(line)
    else:
        print(lvl)
        cur = lvl
        fp.close()
        fp = open(f"db/{lvl}.txt", "w")
        fp.write(line)
