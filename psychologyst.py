from random import choice, choices
from PIL import Image, ImageFont, ImageDraw, ImageColor
import colorsys


from cards import to_puzzle, mk_content, mk_cover, mk_logo_cover, mk_txt, mk_solution, load_db, get_color, mk_descriptions
from card_printing.cards import (
    Card,
    Deck,
    img_merge,
)


def fromuid(h):
    n = int(h, 16)
    l = n.bit_length()
    l -= l % 8 or 8
    lvl = n >> l

    idx = n - (lvl << l)
    return lvl, idx


def touid(lvl, idx):
    # first 8 bits -> lvl
    # rest, also a mulitiple of 8 bits -> idx
    l = idx.bit_length()
    l += 8 - l % 8
    uid = hex(idx + (lvl << l))
    a, b = fromuid(uid)
    assert lvl == a
    assert idx == b
    return uid


def mk_header():
    txt1 = "LEARNING "
    txt2 = "HOUR"
    font = ImageFont.truetype("Myriad Pro Bold.ttf", 75)
    size = Card.SAFE[0]
    header = Image.new("RGBA", (size, 100))
    draw = ImageDraw.Draw(header)
    _, _, tx1, _ = draw.textbbox((0,0), txt1, font=font)
    _, _, tx2, _ = draw.textbbox((0,0), txt2, font=font)
    startx = (size - tx1 - tx2) // 2
    draw.text((startx, 0), txt1, ImageColor.getrgb('#4D6BCB'), font=font)
    draw.text((startx + tx1, 0), txt2, ImageColor.getrgb('#E3463D'), font=font)
    return header


def mk_number(txt):
    font_size = 35
    font = ImageFont.truetype("Myriad Pro Bold.ttf", font_size)
    size = Card.SAFE[0], font_size
    number = Image.new("RGBA", size)
    draw = ImageDraw.Draw(number)
    _, _, tx, ty = draw.textbbox((0,0), txt, font=font)
    red = (206, 54, 56)
    draw.text(((size[0] - tx) // 2, (size[1] - ty) // 2), txt, red, font=font)
    return number


def out_as_byte(value):
    return int(255 * value)


def mk_footer(level, idx, txt):
    colors = {
        1: '#B1D456',
        2: '#34BD51',
        3: '#33B9B9',
        4: '#3B95EF',
        5: '#7189D5',
        6: '#967DD1',
        7: '#C0749B',
        8: '#E96B64',
    }
    # level printed is number of move (db 'level') -2,
    # warmup is the only deck with move below 3, hence the max
    level = max(1, level-2)
    color = colors[level]

    font_size = 50
    font = ImageFont.truetype("Myriad Pro Bold.ttf", font_size)

    # now we know text size, calculate image size
    size = Card.BLEED[0], 3 * font_size + Card.cut_margin[1]
    footer = Image.new("RGBA", size)
    draw = ImageDraw.Draw(footer)
    _, _, tx, ty = draw.textbbox((0,0), txt, font=font)
    py = (size[1] - Card.cut_margin[1] - ty) // 2
    draw.rectangle(((0, 0), size), fill=color)
    draw.text(
        ((size[0] - tx) // 2, py),
        txt,
        (255, 255, 255),
        font=font,
    )

    # temporary uid
    uid = touid(level, idx)
    font = ImageFont.truetype("card_printing/PT_Sans-Narrow-Web-Regular.ttf", 20)
    _, _, tx, ty = draw.textbbox((0,0), uid, font=font)
    draw.text(
        (size[0] - tx - 2, size[1] - ty - 2),
        uid,
        (0, 0, 0),
        font=font,
    )

    # logo
    logo = Image.open("logo.png")
    x, y = logo.size
    # target size is to have y = w-(bleed to cut bot margin)
    #M = (Card.CUT[1]-Card.SAFE[1])//2
    th = 109 #size[1] - 2*M - (Card.CUT[0]-Card.SAFE[0])//2
    ratio = th/y
    logo = logo.resize((int(ratio*x), int(ratio*y)))
    x, y = logo.size
    py = (size[1] - y - Card.cut_margin[1]) // 2
    px = size[0] - x - py - Card.cut_margin[0] - 56//2
    img_merge(footer, logo, (px, py))

    return footer


def mk_logo_footer(level, txt):
    if txt == 'Warmup':
        level -= 2
    color = colors[level][1]
    font_size = 50
    w, h = Card.BLEED[0], 3 * font_size + (Card.BLEED[1] - Card.CUT[1]) // 2
    footer = Image.new("RGBA", (w, h))

    logo = Image.open("logo.png")
    x, y = logo.size
    # target size is to have y = w-(bleed to cut bot margin)
    th = h - Card.safe_margin[1]
    ratio = th/y
    logo = logo.resize((int(ratio*x), int(ratio*y)))
    logo_pos = (Card.BLEED[0] - logo.size[0]) // 2, 0
    img_merge(footer, logo, logo_pos)

    return footer


def gen_cards(repartition=None, preselected=None, deck_id=None, txt=None):
    cache = {}
    selected = []

    if preselected:
        for uid in preselected:
            lvl, idx = fromuid(uid)
            idx -= 1
            if lvl not in cache:
                cache[lvl] = load_db(
                    lvl, limit_car=99, limit_truck=99, limit_wall=99, min_wall=0
                )
            selected.append(cache[lvl][idx])

    for lvl, nb in repartition.items():
        if not nb:
            continue
        if lvl not in cache:
            cache[lvl] = load_db(lvl)

        lvl_selected = []
        while len(lvl_selected) < nb and len(lvl_selected) != len(cache[lvl]):
            new = choice(cache[lvl])
            if new not in lvl_selected:
                lvl_selected.append(new)
        selected.extend(lvl_selected)

    header = mk_header()
    max_cars = 0
    for page_number, (elems, infos) in enumerate(selected, start=1):
        lvl, idx, nb_car, nb_truck, nb_wall, nb_states = infos
        max_cars = max(max_cars, nb_car)
        chars = iter("BCDEFGHIJKLMNOPQRSTUVWXYZ")
        for v in elems:
            if v["pos"][1] == 2 and v["orientation"] == "-":
                v["char"] = "A"
            else:
                v["char"] = next(chars)
        get_color(elems)
        content = mk_content(elems)
        mini_content = content.resize((200, 200))

        if 'Warmup' not in txt:
            txt = f"{lvl-2}{chr(page_number-1+ord('A'))}"
        else:
            txt = f"Warmup - {chr(page_number-1+ord('A'))}"
        footer = mk_footer(lvl, idx, txt)
        solution = mk_solution(elems)
        yield Card(
            parts=[header, content, footer],
            deck_id=deck_id,
            mode="bottom",
            debug='cut',
        ), Card(
            parts=[mini_content, solution, footer],
            deck_id=deck_id,
            mode="bottom",
            debug='cut',
        )
    print('max cars:',max_cars)


warmup_set = [
    "0x101",
    "0x202",
    "0x204",
    "0x205",
    "0x30080",
    "0x300a6",
    "0x3008e",
    "0x40235",
    "0x402da",
    "0x4027d",
    "0x42b",
    "0x51302",
    "0x51c81",
    "0x51dfe",
    "0x65518",
    "0x645ca",
]

# Lvl 3: Can puzzle 13 to 17 be moved to the front. They are easier
lvl3_set = [
    "0x334",  # 13
    "0x335",  # 14
    "0x339",  # 15
    "0x33a",  # 16
    "0x342",  # 17
    "0x303",
    "0x304",
    "0x30b",
    "0x30d",
    "0x31d",
    "0x31f",
    "0x322",
    "0x323",
    "0x328",
    "0x329",
    "0x32d",
    "0x332",
    "0x344",
    "0x34c",
    "0x364",
    "0x369",
    "0x36a",
    "0x36c",
    "0x37f",
    "0x30080",
    "0x30084",
    "0x3008d",
    "0x3008e",
    "0x3009d",
    "0x300a6",
]

# Lvl4: Can these puzzles be put in front in this order: 16, 22, 23, 24, 25, 30
lvl4_set = [
    "0x40235",  # 16
    "0x402da",  # 22
    "0x40365",  # 23
    "0x40384",  # 24
    "0x403a0",  # 25
    "0x40437",  # 30
    "0x42a",
    "0x42b",
    "0x400a7",
    "0x400a9",
    "0x400b7",
    "0x400d9",
    "0x400da",
    "0x400ea",
    "0x400ef",
    "0x40142",
    "0x40148",
    "0x40166",
    "0x401ee",
    "0x401f0",
    "0x4020c",
    "0x4024d",
    "0x40253",
    "0x40276",
    "0x4027d",
    "0x4028c",
    "0x403d9",
    "0x403eb",
    "0x40420",
    "0x40424",
]

# Lvl 5: Can these puzzles be put in front in this order: 10, 15, 29, 23, 22, 21, 30, 25, 24, 7
lvl5_set = [
    "0x509a4",  # 10
    "0x511d7",  # 15
    "0x5274e",  # 29
    "0x51cff",  # 23
    "0x51ca9",  # 22
    "0x51c81",  # 21
    "0x52bd2",  # 30
    "0x51dfe",  # 25
    "0x51d56",  # 24
    "0x506c8",  # 7
    "0x500b8",
    "0x50118",
    "0x50466",
    "0x50476",
    "0x50488",
    "0x505c0",
    "0x50781",
    "0x50834",
    "0x50a46",
    "0x50d99",
    "0x50dc2",
    "0x51066",
    "0x51302",
    "0x514df",
    "0x5166e",
    "0x51832",
    "0x51bea",
    "0x51f45",
    "0x52181",
    "0x5264d",
]

# Lvl 6: Can these puzzles be put in front in this order: 8, 10, 3, 5, 16, 20, 1, 4, 6, 9, 12, 14, 23
lvl6_set = [
    "0x62de3",  # 8
    "0x63273",  # 10
    "0x60830",  # 3
    "0x6170d",  # 5
    "0x64762",  # 16
    "0x65518",  # 20
    "0x6014b",  # 1
    "0x60bb2",  # 4
    "0x62327",  # 6
    "0x62f8f",  # 9
    "0x63d43",  # 12
    "0x64248",  # 14
    "0x659e6",  # 23
    "0x607e8",  # 2
    "0x62a71",  # 7
    "0x63c76",  # 11
    "0x63fb8",  # 13
    "0x645ca",  # 15
    "0x64aa9",  # 17
    "0x64aaf",  # 18
    "0x64f23",  # 19
    "0x656b1",  # 21
    "0x658bc",  # 22
    "0x65ec0",  # 24
    "0x66ee5",  # 25
    "0x67545",  # 26
    "0x67eeb",  # 27
    "0x60082ca",  # 28
    "0x6009fd0",  # 29
    "0x600a396",  # 30
]

# Lvl 7: Can these puzzles be put in front in this order: 9, 12, 15, 14, 4, 5, 7, 10, 3, 18, 22
lvl7_set = [
    "0x7525e",  # 9
    "0x75b51",  # 12
    "0x70081de",  # 15
    "0x76841",  # 14
    "0x72242",  # 4
    "0x722fe",  # 5
    "0x73014",  # 7
    "0x75374",  # 10
    "0x71692",  # 3
    "0x700ca9e",  # 18
    "0x700ff25",  # 22
    "0x70138",  # 1
    "0x70207",  # 2
    "0x72cb1",  # 6
    "0x7306e",  # 8
    "0x75568",  # 11
    "0x75e47",  # 13
    "0x700b07b",  # 16
    "0x700c601",  # 17
    "0x700e9f8",  # 19
    "0x700ee0a",  # 20
    "0x700fe96",  # 21
    "0x7010672",  # 23
    "0x701276f",  # 24
    "0x7012b55",  # 25
    "0x7013f6d",  # 26
    "0x7015165",  # 27
    "0x7015732",  # 28
    "0x70189e3",  # 29
    "0x7019904",  # 30
]

# Lvl 8: Can these puzzles be put in front in this order: 8, 18, 25, 26, 15, 14, 12, 28, 29, 30
lvl8_set = [
    "0x8597a",  # 8
    "0x8016708",  # 18
    "0x8025234",  # 25
    "0x802528b",  # 26
    "0x8014a17",  # 15
    "0x8012272",  # 14
    "0x8008734",  # 12
    "0x8027368",  # 28
    "0x802a89e",  # 29
    "0x802af7a",  # 30
    "0x80108",  # 1
    "0x80f83",  # 2
    "0x82925",  # 3
    "0x82a49",  # 4
    "0x82dc1",  # 5
    "0x83553",  # 6
    "0x83866",  # 7
    "0x87743",  # 9
    "0x87841",  # 10
    "0x8008575",  # 11
    "0x800f589",  # 13
    "0x8014daa",  # 16
    "0x8016207",  # 17
    "0x8016da8",  # 19
    "0x801a6c9",  # 20
    "0x801f839",  # 21
    "0x801ff6f",  # 22
    "0x8024e8d",  # 23
    "0x8024ef3",  # 24
    "0x802619b",  # 27
]


# Lvl 9: Can these puzzles be put in front in this order: 24, 29, 30, 28, 26, 14
lvl9_set = [
    "0x9035a51",  # 24
    "0x903a1ad",  # 29
    "0x903b5f9",  # 30
    "0x90397b1",  # 28
    "0x9037556",  # 26
    "0x901412a",  # 14
    "0x900a3",  # 1
    "0x91488",  # 2
    "0x91493",  # 3
    "0x92b6e",  # 4
    "0x9461f",  # 5
    "0x95aa7",  # 6
    "0x95cce",  # 7
    "0x96312",  # 8
    "0x9636e",  # 9
    "0x968df",  # 10
    "0x97e71",  # 11
    "0x900a3d6",  # 12
    "0x9013c32",  # 13
    "0x9019600",  # 15
    "0x901b7ad",  # 16
    "0x901d527",  # 17
    "0x9020c8a",  # 18
    "0x9026fb5",  # 19
    "0x902a0d6",  # 20
    "0x902ac94",  # 21
    "0x902ae21",  # 22
    "0x902cec4",  # 23
    "0x9035cc7",  # 25
    "0x9038cee",  # 27
]

# Lvl 10: Is fine
lvl10_set = [
    "0xa01a8",  # 1
    "0xa181c",  # 2
    "0xa1f72",  # 3
    "0xa2903",  # 4
    "0xa40cd",  # 5
    "0xa7fa5",  # 6
    "0xa008115",  # 7
    "0xa0083ca",  # 8
    "0xa00923f",  # 9
    "0xa00a2a1",  # 10
    "0xa00a2ca",  # 11
    "0xa00b9e9",  # 12
    "0xa012ce5",  # 13
    "0xa01443d",  # 14
    "0xa014a13",  # 15
    "0xa015bc8",  # 16
    "0xa01c3ee",  # 17
    "0xa01ea14",  # 18
    "0xa023003",  # 19
    "0xa023a8c",  # 20
    "0xa024be9",  # 21
    "0xa02aa5a",  # 22
    "0xa02bc52",  # 23
    "0xa02c3f5",  # 24
    "0xa02e96c",  # 25
    "0xa038d13",  # 26
    "0xa03b0a7",  # 27
    "0xa03f065",  # 28
    "0xa04105d",  # 29
    "0xa0449af",  # 30
]

descr = {
    'Warmup': (
        'The warmup set',
        """This is the warmup set. The purpose of this set is to determine the appropriate complexity
for the child. The set consists of 17 puzzles that quickly increase in difficulty. The easiest
ones are easier than level 1, and the most challenging ones are from level 4.

It is not intended for use in the actual learning situation, and you don't need to use
it if you already know approximately at which level the child is.""",
        "Opvarmningssættet",
        """Dette er opvarmningsættet. Formålet med dette sæt er at finde ud af, hvilken sværhedsgrad
der passer til barnet. Sættet består af 17 opgave som stiger hurtigt i sværhedsgrad.
De nemmeste er nemmere end level 1 og de sværeste er fra level 4.

Det er altså ikke meningen, at du bruger dette sæt i selve læringssituationen og du
behøver heller ikke bruge det, hvis du allerede ved cirka hvilket niveau barnet er på."""
    ),

    "Level 1":(
        "Level 1 and 2",
        """This is level 1 and 2 out of 8. Each level consists of 26 puzzles, but it's not necessary to go through
all 26 before moving on to the next level. The important thing is that you progress to the
next level only when the child is very comfortable solving the tasks at the current level.

Please be aware that the primary learning doesn't occur through trial and error
but instead by having ideas and plans that are tested.""",
        "Level 1 og 2",
        """Dette er level 1 og 2 ud af 8. Hvert level består af 26 opgaver, men man skal ikke nødvendigvis igennem alle 26 før man går videre til næste level. Det vigtige er, at man først går videre når barnet er meget komfortabel med at løse opgaverne for det nuværende level.

Vær opmærksom på, at den primære læring ikke sker ved at prøve sig frem, men i stedet ved at have ideer og planer som man afprøver.""",
    ),

    "Level 3":(
        "Level 3 and 4",
        """This is level 3 and 4 out of 8. Each level consists of 26 puzzles, but it's not necessary to go through
all 26 before moving on to the next level. The important thing is that you progress to the
next level only when the child is very comfortable solving the tasks at the current level.

Please be aware that the primary learning doesn't occur through trial and error
but instead by having ideas and plans that are tested.""",
        "Level 3 og 4",
        """Dette er level 3 og 4 ud af 8. Hvert level består af 26 opgaver, men man skal ikke nødvendigvis igennem alle 26 før man går videre til næste level. Det vigtige er, at man først går videre når barnet er meget komfortabel med at løse opgaverne for det nuværende level.

Vær opmærksom på, at den primære læring ikke sker ved at prøve sig frem, men i stedet ved at have ideer og planer som man afprøver.""",
    ),

    "Level 5":(
        "Level 5 and 6",
        """This is level 5 and 6 out of 8. Each level consists of 26 puzzles, but it's not necessary to go through
all 26 before moving on to the next level. The important thing is that you progress to the
next level only when the child is very comfortable solving the tasks at the current level.

Please be aware that the primary learning doesn't occur through trial and error
but instead by having ideas and plans that are tested.""",
        "Level 5 og 6",
        """Dette er level 5 og 6 ud af 8. Hvert level består af 26 opgaver, men man skal ikke nødvendigvis igennem alle 26 før man går videre til næste level. Det vigtige er, at man først går videre når barnet er meget komfortabel med at løse opgaverne for det nuværende level.

Vær opmærksom på, at den primære læring ikke sker ved at prøve sig frem, men i stedet ved at have ideer og planer som man afprøver.""",
    ),

    "Level 7":(
        "Level 7 and 8",
        """This is level 7 and 8 out of 8. Each level consists of 26 puzzles, but it's not necessary to go through
all 26 before moving on to the next level. The important thing is that you progress to the
next level only when the child is very comfortable solving the tasks at the current level.

Please be aware that the primary learning doesn't occur through trial and error
but instead by having ideas and plans that are tested.""",
        "Level 7 og 8",
        """Dette er level 7 og 8 ud af 8. Hvert level består af 26 opgaver, men man skal ikke nødvendigvis igennem alle 26 før man går videre til næste level. Det vigtige er, at man først går videre når barnet er meget komfortabel med at løse opgaverne for det nuværende level.

Vær opmærksom på, at den primære læring ikke sker ved at prøve sig frem, men i stedet ved at have ideer og planer som man afprøver.""",
    ),
}

sets = [
    # data, name, need_cover
    (warmup_set, "Warmup", True),
    (lvl3_set, "Level 1", True),
    (lvl4_set, "Level 2", False),
    (lvl5_set, "Level 3", True),
    (lvl6_set, "Level 4", False),
    (lvl7_set, "Level 5", True),
    (lvl8_set, "Level 6", False),
    (lvl9_set, "Level 7", True),
    (lvl10_set, "Level 8", False),
]


if __name__ == "__main__":
    for cards_set, name, need_cover in sets:
        d = Deck(name)
        if need_cover:
            for lang in mk_txt():
                d.add(mk_logo_cover())
                d.add(lang)
        description = descr.get(name, None)
        if description:
            for lang in mk_descriptions(description):
                d.add(lang)

        for recto, verso in gen_cards(preselected=cards_set[:26], repartition={}, txt=name):
            d.add(recto)
            d.add(verso)
        d.pdf()
